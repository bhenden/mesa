/**************************************************************************
 *
 * Copyright 2020 VMware, Inc.
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 **************************************************************************/


#include "pipe/p_context.h"

#include "util/u_memory.h"
#include "util/u_math.h"
#include "util/format/u_format.h"
#include "util/format/u_format_yuv.h"
#include "util/u_inlines.h"
#include "util/u_sse.h"
#include "util/u_cpu_detect.h"
#include "util/u_blit_sw.h"


#define MAX_SURFACE_WIDTH 8192

/**
 * Whether to use non temporal fetches
 *
 * Prevents trashing the cache.
 */
#define USE_NON_TEMPORAL_FETCHES 1

/**
 * Whether to use write combining stores with SSE
 *
 * Prevents trashing the cache.
 */
#define USE_WRITE_COMBINING_STORES 1


static uint32_t
round_down(uint32_t value, uint32_t align)
{
   return value / align * align;
}


static uint32_t
round_up(uint32_t value, uint32_t align)
{
   return (value + align - 1) / align * align;
}


struct util_blit_sw_context
{
   uint32_t colororalpha;
   unsigned writemask;

   const struct util_format_description *src_desc;
   const void * restrict src;
   const void * restrict src2;
   uint32_t src_stride;

   const struct util_format_description *dst_desc;
   void * restrict dst;
   uint32_t dst_stride;
   int32_t dst_x0;
   int32_t dst_y0;
   int32_t dst_x1;
   int32_t dst_y1;

   int32_t dst_delta_x;
   int32_t dst_delta_y;

   int32_t fetch_width;

   /* Source start pixel and steps in 16.16 fixed point format */
   int32_t src_x_fp;
   int32_t src_y_fp;
   int32_t src_xstep_fp;
   int32_t src_ystep_fp;

   int32_t clamp_src_x0;
   int32_t clamp_src_x1;
   int32_t clamp_dst_x0;
   int32_t clamp_dst_x1;

   int32_t clamp_y;

   /**
    * Fetch a row of pixel blocks.
    */
   void
   (*fetch_row_8unorm)(struct util_blit_sw_context *ctx,
                       uint32_t * restrict dst_row,
                       const uint8_t * restrict src_row,
                       const uint8_t * restrict src_row2);

   /**
    * Stretch a row of pixels.
    */
   void
   (*stretch_row_8unorm)(struct util_blit_sw_context *ctx,
                         uint32_t * restrict dst_row, int32_t dst_delta_x,
                         const uint32_t * restrict src_row);

   /*
    * Linear interpolate between two rows of pixels.
    */
   void
   (*lerp_row_8unorm)(uint32_t * restrict dst_row,
                      const uint32_t * restrict src0_row,
                      const uint32_t * restrict src1_row,
                      uint32_t weight,
                      uint32_t width);

   /**
    * Store a row of pixel blocks.
    */
   void
   (*store_row_8unorm)(struct util_blit_sw_context *ctx,
                       void * restrict dst_row,
                       const uint32_t * restrict src_row,
                       uint32_t width);

   /*
    * Temporary buffer for pixels after being fetched.
    */

   PIPE_ALIGN_VAR(16) uint32_t
   fetched_row[MAX_SURFACE_WIDTH];

   /*
    * Temporary buffer for stretched rows.
    *
    * We store two rows here which is sufficient for successive
    * interpolations of the same row pair.
    */
   PIPE_ALIGN_VAR(16) uint32_t
   stretched_row[2][MAX_SURFACE_WIDTH];

   /*
    * Temporary buffer for pixels before being stored.
    */
   PIPE_ALIGN_VAR(16) uint32_t
   store_row[MAX_SURFACE_WIDTH];

   int32_t fetched_row_y;

   /**
    * y coordinate of the rows stored in the stretched_row.
    *
    * Negative number means no stretched row is cached.
    */
   int32_t stretched_row_y[2];

   /**
    * The index of stretched_row to receive the next stretched row.
    */
   uint32_t stretched_row_index;
};


/**
 * Fetch a row of pixel blocks.
 */
static void
fetch_row_8unorm(struct util_blit_sw_context *ctx,
                 uint32_t * restrict dst_row,
                 const uint8_t * restrict src_row,
                 const uint8_t * restrict src_row2)
{
   const struct util_format_description *src_desc = ctx->src_desc;

   src_desc->unpack_rgba_8unorm((uint8_t *)dst_row, 0,
                                src_row, 0,
                                ctx->fetch_width, 1);
}


/**
 * Fetch a row of pixels without any conversion.
 */
static void
fetch_row_memcpy(struct util_blit_sw_context *ctx,
                 uint32_t * restrict dst_row,
                 const uint8_t * restrict src_row,
                 const uint8_t * restrict src_row2)
{
   const struct util_format_description *src_desc = ctx->src_desc;

   memcpy(dst_row, src_row, ctx->fetch_width * src_desc->block.bits/8);
}


/**
 * Fetch a row of bgrx8 pixels without swizzling but set alpha to one.
 */
static void
fetch_row_bgrx_noswizzle(struct util_blit_sw_context *ctx,
                         uint32_t * restrict dst_row,
                         const uint8_t * restrict src_row,
                         const uint8_t * restrict src_row2)
{
   ASSERTED const struct util_format_description *src_desc = ctx->src_desc;
   unsigned x;
   const uint32_t * restrict src = (const uint32_t *)src_row;

   assert(src_desc->block.bits == 32);

   for (x = 0; x < ctx->fetch_width; x++) {
      *dst_row++ = *src++ | 0xff000000;
   }
}


#ifdef PIPE_ARCH_SSE

/**
 * Convert from YUV to RGBA8 unorm with SSE2
 */
static inline void
yuv_to_rgb_8unorm_sse2(__m128i y,
                       __m128i uv,
                       __m128i * restrict pixels0,
                       __m128i * restrict pixels1)
{
   __m128i y0, y1;
   __m128i gx, br, rb;
   __m128i gx0, rb0, gx1, rb1;
   __m128i gx0_gx1, ga0_ga1, rb0_rb1;
   __m128i rgba0, rgba1;

   /*
    * YUV -> RGB conversion coefficients.
    *
    * For Y, B, and R instead of doing
    *
    *   (value * coeff) >> 8
    *
    * we do
    *
    *   ((value << 3) * (coeff << 5)) >> 16
    *
    * to avoid having to store intermediate 32bit values. This is inspired from
    * liboggplay's planar YUV -> RGB conversion code.
    */

   const int16_t cy  = 0x253f; /* 298 << 5 approx */
   const int16_t cgu = -100;
   const int16_t cgv = -208;
   const int16_t cbu = 0x4093; /* 516 << 5 approx */
   const int16_t crv = 0x3312; /* 409 << 5 approx */

   /*
    * Offset.
    */

   y  = _mm_sub_epi16(y,  _mm_set1_epi16(16));
   uv = _mm_sub_epi16(uv, _mm_set1_epi16(128));

   /*
    * Force grayscale (only for debugging purposes)
    */

   if (0) {
      uv = _mm_setzero_si128();
   }

   /*
    * Compute luminance.
    */

   y  = _mm_slli_epi16(y, 3);
   y  = _mm_mulhi_epi16(y, _mm_set1_epi16(cy));

   /*
    * Extract and broadcast the even and odd luminance values.
    */

#if 1
   y0 = _mm_shufflelo_epi16(y,  _MM_SHUFFLE(2, 2, 0, 0));
   y0 = _mm_shufflehi_epi16(y0, _MM_SHUFFLE(2, 2, 0, 0));
   y1 = _mm_shufflelo_epi16(y,  _MM_SHUFFLE(3, 3, 1, 1));
   y1 = _mm_shufflehi_epi16(y1, _MM_SHUFFLE(3, 3, 1, 1));
#else
   y0 = _mm_slli_epi32(y, 16);
   y1 = _mm_srli_epi32(y, 16);
   y0 = _mm_or_si128(y0, _mm_srli_epi32(y0, 16));
   y1 = _mm_or_si128(y1, _mm_slli_epi32(y1, 16));
#endif

   /*
    * Compute chrominance.
    */

   gx = _mm_madd_epi16(uv, _mm_setr_epi16(cgu, cgv, cgu, cgv, cgu, cgv, cgu, cgv));
   gx = _mm_srai_epi32(gx, 8);

   uv = _mm_slli_epi16(uv, 3);
   br = _mm_mulhi_epi16(uv, _mm_setr_epi16(cbu, crv, cbu, crv, cbu, crv, cbu, crv));

   /*
    * Swap B and R channels, to faciliate RGBA output.
    */

#if 1
   rb = _mm_shufflelo_epi16(br, _MM_SHUFFLE(2, 3, 0, 1));
   rb = _mm_shufflehi_epi16(rb, _MM_SHUFFLE(2, 3, 0, 1));
#else
   rb = _mm_or_si128(_mm_slli_epi32(br, 16), _mm_srli_epi32(br, 16));
#endif

   /*
    * Add luminance to chrominance.
    */

   gx0 = _mm_adds_epi16(gx, y0);
   rb0 = _mm_adds_epi16(rb, y0);
   gx1 = _mm_adds_epi16(gx, y1);
   rb1 = _mm_adds_epi16(rb, y1);

   /*
    * Pack and saturate to 8 bits.
    */

   gx0_gx1 = _mm_packus_epi16(gx0, gx1);
   rb0_rb1 = _mm_packus_epi16(rb0, rb1);

   /*
    * Set alpha to one.
    */

   ga0_ga1 = _mm_or_si128(gx0_gx1, _mm_set1_epi16(0xff00));

   /*
    * Interleave into the right order.
    */

   rgba0 = _mm_unpacklo_epi8(rb0_rb1, ga0_ga1);
   rgba1 = _mm_unpackhi_epi8(rb0_rb1, ga0_ga1);

   /*
    * rgba0 = r0 g0 b0 a0 r2 g2 b2 a2 r4 g4 b4 a4 r6 g6 b6 a6
    * rgba1 = r1 g1 b1 a1 r3 g3 b3 a3 r5 g5 b5 a5 r7 g7 b7 a7
    */

   *pixels0 = _mm_unpacklo_epi32(rgba0, rgba1);
   *pixels1 = _mm_unpackhi_epi32(rgba0, rgba1);

   /*
    * pixels0 = r0 g0 b0 a0 r1 g1 b1 a1 r2 g2 b2 a2 r3 g3 b3 a3
    * pixels1 = r4 g4 b4 a4 r5 g5 b5 a5 r6 g6 b6 a6 r7 g7 b7 a7
    */
}


/**
 * Fast SSE2 path to fetch a row of YUYV blocks.
 *
 * See also util_format_uyvy_unpack_rgba_8unorm().
 */
static void
fetch_row_8unorm_yuyv_sse2(struct util_blit_sw_context *ctx,
                           uint32_t * restrict dst_row,
                           const uint8_t * restrict src_row,
                           const uint8_t * restrict src_row2)
{
   uint32_t width = ctx->fetch_width;
   uint8_t *dst = (uint8_t *)dst_row;
   const uint32_t *src = (const uint32_t *)src_row;

   assert(width % 2 == 0);

   while (width >= 2 && ((uintptr_t)src & 0xf)) {
      uint32_t value = *src++;

      uint8_t y0 = (value >>  0) & 0xff;
      uint8_t u  = (value >>  8) & 0xff;
      uint8_t y1 = (value >> 16) & 0xff;
      uint8_t v  = (value >> 24) & 0xff;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }

   while (width >= 8) {
      __m128i values;
      __m128i y, uv;
      __m128i rgba0;
      __m128i rgba1;

#if USE_NON_TEMPORAL_FETCHES
      _mm_prefetch((const char *)src + sizeof(__m128i), _MM_HINT_NTA);
#endif

      values = _mm_load_si128((const __m128i *)src);
      src += 4;

      y  = _mm_and_si128(values, _mm_set1_epi16(0xff));
      uv = _mm_srli_epi16(values, 8);

      yuv_to_rgb_8unorm_sse2(y, uv, &rgba0, &rgba1);

      _mm_storeu_si128((__m128i *)dst, rgba0);
      dst += 16;

      _mm_storeu_si128((__m128i *)dst, rgba1);
      dst += 16;

      width -= 8;
   }

   while (width >= 2) {
      uint32_t value = *src++;

      uint8_t y0 = (value >>  0) & 0xff;
      uint8_t u  = (value >>  8) & 0xff;
      uint8_t y1 = (value >> 16) & 0xff;
      uint8_t v  = (value >> 24) & 0xff;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }
}

/**
 * Fast SSE2 path to fetch a row of UYVY blocks.
 *
 * See also util_format_uyvy_unpack_rgba_8unorm().
 */
static void
fetch_row_8unorm_uyvy_sse2(struct util_blit_sw_context *ctx,
                           uint32_t * restrict dst_row,
                           const uint8_t * restrict src_row,
                           const uint8_t * restrict src_row2)
{
   uint32_t width = ctx->fetch_width;
   uint8_t *dst = (uint8_t *)dst_row;
   const uint32_t *src = (const uint32_t *)src_row;

   assert(width % 2 == 0);

   while (width >= 2 && ((uintptr_t)src & 0xf)) {
      uint32_t value = *src++;

      uint8_t u  = (value >>  0) & 0xff;
      uint8_t y0 = (value >>  8) & 0xff;
      uint8_t v  = (value >> 16) & 0xff;
      uint8_t y1 = (value >> 24) & 0xff;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }

   while (width >= 8) {
      __m128i values;
      __m128i y, uv;
      __m128i rgba0;
      __m128i rgba1;

#if USE_NON_TEMPORAL_FETCHES
      _mm_prefetch((const char *)src + sizeof(__m128i), _MM_HINT_NTA);
#endif

      values = _mm_load_si128((const __m128i *)src);
      src += 4;

      uv = _mm_and_si128(values, _mm_set1_epi16(0xff));
      y = _mm_srli_epi16(values, 8);

      yuv_to_rgb_8unorm_sse2(y, uv, &rgba0, &rgba1);

      _mm_storeu_si128((__m128i *)dst, rgba0);
      dst += 16;

      _mm_storeu_si128((__m128i *)dst, rgba1);
      dst += 16;

      width -= 8;
   }

   while (width >= 2) {
      uint32_t value = *src++;

      uint8_t u  = (value >>  0) & 0xff;
      uint8_t y0 = (value >>  8) & 0xff;
      uint8_t v  = (value >> 16) & 0xff;
      uint8_t y1 = (value >> 24) & 0xff;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }
}

/**
 * Fast SSE2 path to fetch a row of nv12 data (from 2 src).
 */
static void
fetch_row_8unorm_nv12_sse2(struct util_blit_sw_context *ctx,
                           uint32_t * restrict dst_row,
                           const uint8_t * restrict src_row,
                           const uint8_t * restrict src_row2)
{
   uint32_t width = ctx->fetch_width;
   uint8_t *dst = (uint8_t *)dst_row;
   const uint16_t *src = (const uint16_t *)src_row;
   const uint16_t *src2 = (const uint16_t *)src_row2;
   __m128i zero = _mm_setzero_si128();


   assert(width % 2 == 0);

   while (width >= 2 && ((uintptr_t)src & 0x7)) {
      uint16_t value = *src++;
      uint16_t valueuv = *src2++;

      uint8_t y0 = value & 0xff;
      uint8_t y1 = value >> 8;
      uint8_t u  = valueuv & 0xff;
      uint8_t v  = valueuv >> 8;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }

   while (width >= 8) {
      __m128i y, uv;
      __m128i rgba0;
      __m128i rgba1;

#if USE_NON_TEMPORAL_FETCHES
      _mm_prefetch((const char *)src + sizeof(__m128i), _MM_HINT_NTA);
      _mm_prefetch((const char *)src2 + sizeof(__m128i), _MM_HINT_NTA);
#endif

      /*
       * Alternatively, could use a width >= 16 loop and adjust things
       * accordingly.
       * Note _mm_loadl_epi64 is sort of broken by design (the prototype
       * is wrong as it should take a void *). With some luck we don't
       * hit one of the issues compilers have with it...
       */
      y =_mm_loadl_epi64((const __m128i *)src);
      uv = _mm_loadl_epi64((const __m128i *)src2);
      src += 4;
      src2 += 4;

      y  = _mm_unpacklo_epi8(y, zero);
      uv = _mm_unpacklo_epi8(uv, zero);

      yuv_to_rgb_8unorm_sse2(y, uv, &rgba0, &rgba1);

      _mm_storeu_si128((__m128i *)dst, rgba0);
      dst += 16;

      _mm_storeu_si128((__m128i *)dst, rgba1);
      dst += 16;

      width -= 8;
   }

   while (width >= 2) {
      uint16_t value = *src++;
      uint16_t valueuv = *src2++;

      uint8_t y0 = value & 0xff;
      uint8_t y1 = value >> 8;
      uint8_t u  = valueuv & 0xff;
      uint8_t v  = valueuv >> 8;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }
}

#endif /* PIPE_ARCH_SSE */

/**
 * Fetch a row of nv12 data (from 2 src).
 */
static void
fetch_row_8unorm_nv12(struct util_blit_sw_context *ctx,
                      uint32_t * restrict dst_row,
                      const uint8_t * restrict src_row,
                      const uint8_t * restrict src_row2)
{
   uint32_t width = ctx->fetch_width;
   uint8_t *dst = (uint8_t *)dst_row;
   const uint16_t *src = (const uint16_t *)src_row;
   const uint16_t *src2 = (const uint16_t *)src_row2;

   assert(width % 2 == 0);

   while (width >= 2) {
      uint16_t value = *src++;
      uint16_t valueuv = *src2++;

      uint8_t y0 = value & 0xff;
      uint8_t y1 = value >> 8;
      uint8_t u  = valueuv & 0xff;
      uint8_t v  = valueuv >> 8;

      util_format_yuv_to_rgb_8unorm(y0, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      util_format_yuv_to_rgb_8unorm(y1, u, v, &dst[0], &dst[1], &dst[2]);
      dst[3] = 0xff; /* a */
      dst += 4;

      width -= 2;
   }
}

/**
 * Linear interpolation of rgba8 unorm pixels.
 *
 * Using the double blend trick documented in
 * http://www.stereopsis.com/doubleblend.html
 *
 * @param weight should be between in [0, 255], where 256 corresponds to unity
 */
static ALWAYS_INLINE void
lerp_pixel_8unorm(uint32_t *dst,
                  uint32_t src0, uint32_t src1,
                  uint32_t weight)
{
   uint32_t src0_rb =  src0       & 0x00ff00ff;
   uint32_t src0_ag = (src0 >> 8) & 0x00ff00ff;
   uint32_t src1_rb =  src1       & 0x00ff00ff;
   uint32_t src1_ag = (src1 >> 8) & 0x00ff00ff;

   uint32_t delta_rb = src1_rb - src0_rb;
   uint32_t delta_ag = src1_ag - src0_ag;

   uint32_t dst_rb;
   uint32_t dst_ag;

   delta_rb *= weight;
   delta_ag *= weight;
   delta_rb >>= 8;
   delta_ag >>= 8;

   dst_rb  = (delta_rb + src0_rb) & 0x00ff00ff;
   dst_ag  = (delta_ag + src0_ag) & 0x00ff00ff;

   *dst = dst_rb | (dst_ag << 8);
}


/**
 * Stretch a row of pixels using nearest filter.
 *
 * Uses Bresenham's line algorithm using 16.16 fixed point representation for
 * the error term.
 */
static void
nearest_stretch_row_8unorm(struct util_blit_sw_context *ctx,
                           uint32_t * restrict dst_row, int32_t dst_delta_x,
                           const uint32_t * restrict src_row)
{
   int32_t src_x_fp = ctx->src_x_fp;
   int32_t src_xstep_fp = ctx->src_xstep_fp;
   int32_t clamp_src_x0 = ctx->clamp_src_x0;
   int32_t clamp_src_x1 = ctx->clamp_src_x1;
   int32_t clamp_dst_x0 = ctx->clamp_dst_x0;
   int32_t clamp_dst_x1 = ctx->clamp_dst_x1;
   int32_t error;
   int32_t dst_x, src_x;

   dst_x = 0;

   src_x_fp += 0x8000;
   src_x = src_x_fp >> 16;
   error = src_x_fp & 0xffff;

   while (dst_x < clamp_dst_x0) {
      dst_row[dst_x] = src_row[clamp_src_x0];

      ++dst_x;
      error += src_xstep_fp;
      src_x += error >> 16;
      error &= 0xffff;
   }

   while (dst_x < clamp_dst_x1) {
      assert(src_x >= 0 && src_x < ctx->fetch_width);

      dst_row[dst_x] = src_row[src_x];

      ++dst_x;
      error += src_xstep_fp;
      src_x += error >> 16;
      error &= 0xffff;
   }

   while (dst_x < dst_delta_x) {
      dst_row[dst_x] = src_row[clamp_src_x1];

      ++dst_x;
   }
}


/**
 * As above, but 16-bit pixels.
 */
static void
nearest_stretch_row_16(struct util_blit_sw_context *ctx,
                       uint32_t * restrict dst_row, int32_t dst_delta_x,
                       const uint32_t * restrict src_row)
{
   int32_t src_x_fp = ctx->src_x_fp;
   int32_t src_xstep_fp = ctx->src_xstep_fp;
   int32_t clamp_src_x0 = ctx->clamp_src_x0;
   int32_t clamp_src_x1 = ctx->clamp_src_x1;
   int32_t clamp_dst_x0 = ctx->clamp_dst_x0;
   int32_t clamp_dst_x1 = ctx->clamp_dst_x1;
   int32_t error;
   int32_t dst_x, src_x;
   uint16_t * restrict dst_row16 = (uint16_t *) dst_row;
   const uint16_t * restrict src_row16 = (const uint16_t *) src_row;

   dst_x = 0;

   src_x_fp += 0x8000;
   src_x = src_x_fp >> 16;
   error = src_x_fp & 0xffff;

   while (dst_x < clamp_dst_x0) {
      dst_row16[dst_x] = src_row16[clamp_src_x0];

      ++dst_x;
      error += src_xstep_fp;
      src_x += error >> 16;
      error &= 0xffff;
   }

   while (dst_x < clamp_dst_x1) {
      assert(src_x >= 0 && src_x < ctx->fetch_width);

      dst_row16[dst_x] = src_row16[src_x];

      ++dst_x;
      error += src_xstep_fp;
      src_x += error >> 16;
      error &= 0xffff;
   }

   while (dst_x < dst_delta_x) {
      dst_row16[dst_x] = src_row16[clamp_src_x1];

      ++dst_x;
   }
}


/**
 * As above, but 64-bit pixels.
 */
static void
nearest_stretch_row_64(struct util_blit_sw_context *ctx,
                       uint32_t * restrict dst_row, int32_t dst_delta_x,
                       const uint32_t * restrict src_row)
{
   int32_t src_x_fp = ctx->src_x_fp;
   int32_t src_xstep_fp = ctx->src_xstep_fp;
   int32_t clamp_src_x0 = ctx->clamp_src_x0;
   int32_t clamp_src_x1 = ctx->clamp_src_x1;
   int32_t clamp_dst_x0 = ctx->clamp_dst_x0;
   int32_t clamp_dst_x1 = ctx->clamp_dst_x1;
   int32_t error;
   int32_t dst_x, src_x;
   uint64_t * restrict dst_row64 = (uint64_t *) dst_row;
   const uint64_t * restrict src_row64 = (const uint64_t *) src_row;

   dst_x = 0;

   src_x_fp += 0x8000;
   src_x = src_x_fp >> 16;
   error = src_x_fp & 0xffff;

   while (dst_x < clamp_dst_x0) {
      dst_row64[dst_x] = src_row64[clamp_src_x0];

      ++dst_x;
      error += src_xstep_fp;
      src_x += error >> 16;
      error &= 0xffff;
   }

   while (dst_x < clamp_dst_x1) {
      assert(src_x >= 0 && src_x < ctx->fetch_width);

      dst_row64[dst_x] = src_row64[src_x];

      ++dst_x;
      error += src_xstep_fp;
      src_x += error >> 16;
      error &= 0xffff;
   }

   while (dst_x < dst_delta_x) {
      dst_row64[dst_x] = src_row64[clamp_src_x1];

      ++dst_x;
   }
}


/**
 * Stretch a row of pixels using linear filter.
 *
 * Uses Bresenham's line algorithm using 16.16 fixed point representation for
 * the error term.
 */
static void
linear_stretch_row_8unorm(struct util_blit_sw_context *ctx,
                          uint32_t * restrict dst_row, int32_t dst_delta_x,
                          const uint32_t * restrict src_row)
{
   int32_t clamp_src_x0 = ctx->clamp_src_x0;
   int32_t clamp_src_x1 = ctx->clamp_src_x1;
   int32_t clamp_dst_x0 = ctx->clamp_dst_x0;
   int32_t clamp_dst_x1 = ctx->clamp_dst_x1;

   int32_t aligned_clamp_dst_x0 = (clamp_dst_x0 + 3) & ~3;
   int32_t aligned_clamp_dst_x1 =  clamp_dst_x1      & ~3;

   int32_t src_x_fp = ctx->src_x_fp;
   int32_t src_xstep_fp = ctx->src_xstep_fp;
   int32_t dst_x = 0;

   /* Texels clamped against the left edge */
   while (dst_x < clamp_dst_x0) {
      dst_row[dst_x] = src_row[clamp_src_x0];
      src_x_fp += src_xstep_fp;
      ++dst_x;
   }

   /* Fast SSE path for the inner aligned pixels */
#ifdef PIPE_ARCH_SSE
   if (aligned_clamp_dst_x0 < aligned_clamp_dst_x1) {
      /* Pixels between the clamped left edge and the next aligned column */
      while (dst_x < aligned_clamp_dst_x0) {
         int32_t src_x0 = src_x_fp >> 16;
         int32_t src_x1 = src_x0 + 1;
         uint32_t weight = (src_x_fp >> 8) & 0xff;

         assert(src_x0 >= 0 && src_x0 < ctx->fetch_width);
         assert(src_x1 >= 0 && src_x1 < ctx->fetch_width);

         lerp_pixel_8unorm(&dst_row[dst_x],
                           src_row[src_x0],
                           src_row[src_x1],
                           weight);

         src_x_fp += src_xstep_fp;
         ++dst_x;
      }

      /* Aligned, non-clamped pixels */
      src_x_fp = util_sse2_stretch_row_8unorm((__m128i *)&dst_row[aligned_clamp_dst_x0],
                                              aligned_clamp_dst_x1 - aligned_clamp_dst_x0,
                                              src_row, src_x_fp, src_xstep_fp);
      dst_x = aligned_clamp_dst_x1;
   }
#else
   (void)aligned_clamp_dst_x0;
   (void)aligned_clamp_dst_x1;
#endif

   /* Pixels until the right clamp edge */
   while (dst_x < clamp_dst_x1) {
      int32_t src_x0 = src_x_fp >> 16;
      int32_t src_x1 = src_x0 + 1;
      uint32_t weight = (src_x_fp >> 8) & 0xff;

      assert(src_x0 >= 0 && src_x0 < ctx->fetch_width);
      assert(src_x1 >= 0 && src_x1 < ctx->fetch_width);

      lerp_pixel_8unorm(&dst_row[dst_x],
                        src_row[src_x0],
                        src_row[src_x1],
                        weight);

      src_x_fp += src_xstep_fp;
      ++dst_x;
   }

   /* Pixels clamped against the right edge */
   while (dst_x < dst_delta_x) {
      dst_row[dst_x] = src_row[clamp_src_x1];
      ++dst_x;
   }
}


/**
 * Fetch and stretch one row.
 */
static inline uint32_t *
fetch_and_stretch_row_8unorm(struct util_blit_sw_context *ctx,
                             int32_t src_y)
{
   const struct util_format_description *src_desc = ctx->src_desc;
   const struct util_format_block *block = &src_desc->block;
   const void * restrict src = ctx->src;
   uint32_t * restrict dst_row;
   int32_t dst_delta_x = ctx->dst_delta_x;
   uint32_t src_stride = ctx->src_stride;
   uint32_t * restrict tmp_row = ctx->fetched_row;
   const uint8_t * restrict src_row;
   const uint8_t * restrict src2_row;

   /*
    * Search the stretched row cache first.
    */

   if (src_y == ctx->stretched_row_y[0]) {
      ctx->stretched_row_index = 1;
      return ctx->stretched_row[0];
   }

   if (src_y == ctx->stretched_row_y[1]) {
      ctx->stretched_row_index = 0;
      return ctx->stretched_row[1];
   }

   /*
    * Replace one entry.
    */

   src_row = (const uint8_t *)src + src_stride*(src_y/block->height);

   dst_row = ctx->stretched_row[ctx->stretched_row_index];

   if (ctx->src2) {
      assert(src_stride % 2 == 0);
      src2_row = (const uint8_t *)ctx->src2 + src_stride*(src_y/2);
   }

   if (ctx->src_x_fp == 0 && ctx->src_xstep_fp == 0x10000) {
      ctx->fetch_row_8unorm(ctx, dst_row, src_row, src2_row);
   }
   else {
      ctx->fetch_row_8unorm(ctx, tmp_row, src_row, src2_row);
      ctx->stretch_row_8unorm(ctx,
                              dst_row, dst_delta_x, tmp_row);
   }

   ctx->stretched_row_y[ctx->stretched_row_index] = src_y;
   ctx->stretched_row_index ^= 1;

   return dst_row;
}


/**
 * Linear interpolate two rows of pixels.
 */
static void
lerp_row_8unorm(uint32_t * restrict dst_row,
                const uint32_t * restrict src0_row,
                const uint32_t * restrict src1_row,
                uint32_t weight,
                uint32_t width)
{
   uint32_t x;

   for (x = 0; x < width; ++x) {
      lerp_pixel_8unorm(&dst_row[x],
                        src0_row[x],
                        src1_row[x],
                        weight);
   }
}


#ifdef PIPE_ARCH_SSE

/**
 * Linear interpolate two rows of pixels with SSE2.
 */
static void
lerp_row_8unorm_sse2(uint32_t * restrict dst_row,
                     const uint32_t * restrict src0_row,
                     const uint32_t * restrict src1_row,
                     uint32_t weight,
                     uint32_t width)
{
   __m128i weight_vec = _mm_set1_epi16(weight);
   __m128i dst, src0, src1;
   uint32_t x;

   x = 0;
   if (((uintptr_t)dst_row | (uintptr_t)src0_row | (uintptr_t)src1_row) & 0xf) {
      while (x + 4 <= width) {
         src0 = _mm_loadu_si128((const __m128i *)&src0_row[x]);
         src1 = _mm_loadu_si128((const __m128i *)&src1_row[x]);

         dst = util_sse2_lerp_epi8_fixed88(src0, src1,
                                           &weight_vec, &weight_vec);

         _mm_storeu_si128((__m128i *)&dst_row[x], dst);
         x += 4;
      }
   } else {
      while (x + 4 <= width) {
         src0 = _mm_load_si128((const __m128i *)&src0_row[x]);
         src1 = _mm_load_si128((const __m128i *)&src1_row[x]);

         dst = util_sse2_lerp_epi8_fixed88(src0, src1,
                                           &weight_vec, &weight_vec);

         _mm_store_si128((__m128i *)&dst_row[x], dst);
         x += 4;
      }
   }

   while (x < width) {
      lerp_pixel_8unorm(&dst_row[x],
                        src0_row[x],
                        src1_row[x],
                        weight);
      ++x;
   }
}

#endif /* PIPE_ARCH_SSE */


/**
 * Store one row of pixels.
 */
static void
store_row_8unorm(struct util_blit_sw_context *ctx,
                 void * restrict dst_row,
                 const uint32_t * restrict src_row,
                 uint32_t width)
{
   const struct util_format_description *dst_desc = ctx->dst_desc;

   dst_desc->pack_rgba_8unorm(dst_row, 0,
                              (const uint8_t *)src_row, 0,
                              width, 1);
}


/**
 * Store one row of pixels, without conversion.
 */
static void
store_row_memcpy(struct util_blit_sw_context *ctx,
                 void * restrict dst_row,
                 const uint32_t * restrict src_row,
                 uint32_t width)
{
   const struct util_format_description *dst_desc = ctx->dst_desc;

   memcpy(dst_row, src_row, width * dst_desc->block.bits/8);
}


/**
 * Fast path for storing one row of PIPE_FORMAT_B8G8R8A8_UNORM pixels.
 */
static void
store_row_8unorm_bgra(struct util_blit_sw_context *ctx,
                      void * restrict dst_row,
                      const uint32_t * restrict src_row,
                      uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;

      *dst++ =  (src_pixel & 0xff00ff00)
             | ((src_pixel & 0x000000ff) << 16)
             | ((src_pixel & 0x00ff0000) >> 16);
   }
}

/*
 * Implements one/inv_src_alpha blend.
 * *dst = *dst * (invsrc0a) + src0
 */
static ALWAYS_INLINE void
blend_oneinvsrcalpha_pixel_8unorm(uint32_t *dst,
                                  uint32_t src0)
{
   /*
    * This looks like very slow c code but it ain't a lerp.
    */

   uint32_t src0_a = src0 >> 24;
   uint32_t src0_r = (src0 >> 16) & 0xff;
   uint32_t src0_g = (src0 >> 8) & 0xff;
   uint32_t src0_b = src0 & 0xff;
   uint32_t dst_a = *dst >> 24;
   uint32_t dst_r = (*dst >> 16) & 0xff;
   uint32_t dst_g = (*dst >> 8) & 0xff;
   uint32_t dst_b = *dst & 0xff;
   uint32_t invalpha = 255 - src0_a;

   dst_a *= invalpha;
   dst_r *= invalpha;
   dst_g *= invalpha;
   dst_b *= invalpha;
   /* the shift will cause some precision loss */
   dst_a >>= 8;
   dst_r >>= 8;
   dst_g >>= 8;
   dst_b >>= 8;

   dst_a += src0_a;
   dst_r += src0_r;
   dst_g += src0_g;
   dst_b += src0_b;

   if (dst_a > 0xff)
      dst_a = 0xff;
   if (dst_r > 0xff)
      dst_r = 0xff;
   if (dst_g > 0xff)
      dst_g = 0xff;
   if (dst_b > 0xff)
      dst_b = 0xff;

   *dst = (dst_a << 24) | (dst_r << 16) | (dst_g << 8) | dst_b;
}


/*
 * premultiplies src with constant alpha then
 * does one/inv_src_alpha blend.
 * Implements one/inv_src_alpha blend.
 * *dst = *dst * (invsrc0a) + src0
 */
static ALWAYS_INLINE void
blend_srcalpha_premul_pixel_8unorm(uint32_t *dst,
                                   uint32_t src0,
                                   uint32_t const_alpha)
{
   /*
    * This looks like very slow c code but it ain't a lerp.
    */

   uint32_t src0_a = src0 >> 24;
   uint32_t src0_r = (src0 >> 16) & 0xff;
   uint32_t src0_g = (src0 >> 8) & 0xff;
   uint32_t src0_b = src0 & 0xff;
   uint32_t dst_a = *dst >> 24;
   uint32_t dst_r = (*dst >> 16) & 0xff;
   uint32_t dst_g = (*dst >> 8) & 0xff;
   uint32_t dst_b = *dst & 0xff;
   uint32_t invalpha;

   src0_a *= const_alpha;
   src0_r *= const_alpha;
   src0_g *= const_alpha;
   src0_b *= const_alpha;
   /* the shift will cause some precision loss */
   src0_a >>= 8;
   src0_r >>= 8;
   src0_g >>= 8;
   src0_b >>= 8;

   invalpha = 255 - src0_a;

   dst_a *= invalpha;
   dst_r *= invalpha;
   dst_g *= invalpha;
   dst_b *= invalpha;
   /* the shift will cause some precision loss */
   dst_a >>= 8;
   dst_r >>= 8;
   dst_g >>= 8;
   dst_b >>= 8;

   dst_a += src0_a;
   dst_r += src0_r;
   dst_g += src0_g;
   dst_b += src0_b;

   if (dst_a > 0xff)
      dst_a = 0xff;
   if (dst_r > 0xff)
      dst_r = 0xff;
   if (dst_g > 0xff)
      dst_g = 0xff;
   if (dst_b > 0xff)
      dst_b = 0xff;

   *dst = (dst_a << 24) | (dst_r << 16) | (dst_g << 8) | dst_b;
}


/**
 * Store one row with clear (inefficient if we get here).
 */
static void
store_row_8unorm_bgra_clear(struct util_blit_sw_context *ctx,
                            void * restrict dst_row,
                            const uint32_t * restrict src_row,
                            uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      *dst++ = 0;
   }
}

/**
 * Store one row of bgra pixels with nor.
 */
static void
store_row_8unorm_bgra_nor(struct util_blit_sw_context *ctx,
                          void * restrict dst_row,
                          const uint32_t * restrict src_row,
                          uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = ~(src_pixel | tmp);
   }
}

/**
 * Store one row of bgra pixels with and_inverted.
 */
static void
store_row_8unorm_bgra_and_inverted(struct util_blit_sw_context *ctx,
                                   void * restrict dst_row,
                                   const uint32_t * restrict src_row,
                                   uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = ~src_pixel & tmp;
   }
}

/**
 * Store one row of bgra pixels with copy_inverted.
 */
static void
store_row_8unorm_bgra_copy_inverted(struct util_blit_sw_context *ctx,
                                    void * restrict dst_row,
                                    const uint32_t * restrict src_row,
                                    uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;
      *dst++ = ~src_pixel;
   }
}

/**
 * Store one row of bgra pixels with and_reverse.
 */
static void
store_row_8unorm_bgra_and_reverse(struct util_blit_sw_context *ctx,
                                  void * restrict dst_row,
                                  const uint32_t * restrict src_row,
                                  uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = src_pixel & ~tmp;
   }
}

/**
 * Store one row of bgra pixels with invert (inefficient if we get here).
 */
static void
store_row_8unorm_bgra_invert(struct util_blit_sw_context *ctx,
                             void * restrict dst_row,
                             const uint32_t * restrict src_row,
                             uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      *dst++ = ~tmp;
   }
}

/**
 * Store one row of bgra pixels with xor.
 */
static void
store_row_8unorm_bgra_xor(struct util_blit_sw_context *ctx,
                          void * restrict dst_row,
                          const uint32_t * restrict src_row,
                          uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = src_pixel ^ tmp;
   }
}

/**
 * Store one row of bgra pixels with nand.
 */
static void
store_row_8unorm_bgra_nand(struct util_blit_sw_context *ctx,
                           void * restrict dst_row,
                           const uint32_t * restrict src_row,
                           uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = ~(src_pixel & tmp);
   }
}

/**
 * Store one row of bgra pixels with and.
 */
static void
store_row_8unorm_bgra_and(struct util_blit_sw_context *ctx,
                          void * restrict dst_row,
                          const uint32_t * restrict src_row,
                          uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = src_pixel & tmp;
   }
}

/**
 * Store one row of bgra pixels with equiv (nxor).
 */
static void
store_row_8unorm_bgra_equiv(struct util_blit_sw_context *ctx,
                            void * restrict dst_row,
                            const uint32_t * restrict src_row,
                            uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = ~(src_pixel ^ tmp);
   }
}

/**
 * Store one row with noop (inefficient if we get here).
 */
static void
store_row_8unorm_bgra_noop(struct util_blit_sw_context *ctx,
                           void * restrict dst_row,
                           const uint32_t * restrict src_row,
                           uint32_t width)
{
}

/**
 * Store one row of bgra pixels with or_inverted.
 */
static void
store_row_8unorm_bgra_or_inverted(struct util_blit_sw_context *ctx,
                                  void * restrict dst_row,
                                  const uint32_t * restrict src_row,
                                  uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = ~src_pixel | tmp;
   }
}

/**
 * Store one row of bgra pixels with or_reverse.
 */
static void
store_row_8unorm_bgra_or_reverse(struct util_blit_sw_context *ctx,
                                 void * restrict dst_row,
                                 const uint32_t * restrict src_row,
                                 uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = src_pixel | ~tmp;
   }
}

/**
 * Store one row of bgra pixels with or.
 */
static void
store_row_8unorm_bgra_or(struct util_blit_sw_context *ctx,
                         void * restrict dst_row,
                         const uint32_t * restrict src_row,
                         uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t tmp = *dst;
      uint32_t src_pixel = *src_row++;
      *dst++ = src_pixel | tmp;
   }
}

/**
 * Store one row with set (inefficient if we get here).
 */
static void
store_row_8unorm_bgra_set(struct util_blit_sw_context *ctx,
                          void * restrict dst_row,
                          const uint32_t * restrict src_row,
                          uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      *dst++ = 0xffffffff;
   }
}

#ifdef PIPE_ARCH_SSE

static void
store_row_8unorm_bgra_alphablend_cst_sse2(struct util_blit_sw_context *ctx,
                                          void * restrict dst_row,
                                          const uint32_t * restrict src_row,
                                          uint32_t width)
{
   uint32_t cst_alpha = ctx->colororalpha;
   __m128i weight_vec = _mm_set1_epi16(cst_alpha);
   __m128i dst, src;
   uint32_t x = 0;
   uint32_t * restrict dst32 = (uint32_t *)dst_row;

   /*
    * most likely src and dst don't have same alignment so just always make
    * dst 128bit-aligned and assume src unaligned.
    */

   while ((((uintptr_t)dst32) & 0xf) && (x < width)) {
      lerp_pixel_8unorm(dst32,
                        *dst32,
                        src_row[x],
                        cst_alpha);
      dst32++;
      x++;
   }

   while (x + 4 <= width) {
      src = _mm_loadu_si128((const __m128i *)&src_row[x]);
      dst = _mm_load_si128((const __m128i *)dst32);
      dst = util_sse2_lerp_epi8_fixed88(dst, src,
                                        &weight_vec, &weight_vec);
      _mm_store_si128((__m128i *)dst32, dst);
      dst32 += 4;
      x += 4;
   }

   while (x < width) {
      lerp_pixel_8unorm(dst32,
                        *dst32,
                        src_row[x],
                        cst_alpha);
      dst32++;
      x++;
   }
}

#endif /* PIPE_ARCH_SSE */


/**
 * Store one row of bgra pixels with alpha blend using constant alpha.
 */
static void
store_row_8unorm_bgra_alphablend_cst(struct util_blit_sw_context *ctx,
                                     void * restrict dst_row,
                                     const uint32_t * restrict src_row,
                                     uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;
   uint32_t cst_alpha = ctx->colororalpha;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;
      lerp_pixel_8unorm(dst,
                        *dst,
                        src_pixel,
                        cst_alpha);
      dst++;
   }
}


#ifdef PIPE_ARCH_SSE

static void
store_row_8unorm_bgra_alphablend_src_sse2(struct util_blit_sw_context *ctx,
                                          void * restrict dst_row,
                                          const uint32_t * restrict src_row,
                                          uint32_t width)
{
   uint32_t x = 0;
   uint32_t * restrict dst32 = (uint32_t *)dst_row;
   __m128i dst, src;

   while ((((uintptr_t)dst32) & 0xf) && (x < width)) {
      blend_oneinvsrcalpha_pixel_8unorm(dst32, src_row[x]);
      dst32++;
      x++;
   }

   while (x + 4 <= width) {
      src = _mm_loadu_si128((const __m128i *)&src_row[x]);
      dst = _mm_load_si128((const __m128i *)dst32);
      dst = util_sse2_blend_premul_4(src, dst);
      _mm_store_si128((__m128i *)dst32, dst);
      dst32 += 4;
      x += 4;
   }

   while (x < width) {
      blend_oneinvsrcalpha_pixel_8unorm(dst32, src_row[x]);
      dst32++;
      x++;
   }
}

#endif /* PIPE_ARCH_SSE */


/**
 * Store one row of bgra pixels with alpha blend using src alpha.
 */
static void
store_row_8unorm_bgra_alphablend_src(struct util_blit_sw_context *ctx,
                                     void * restrict dst_row,
                                     const uint32_t * restrict src_row,
                                     uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;
      blend_oneinvsrcalpha_pixel_8unorm(dst, src_pixel);
      dst++;
   }
}


#ifdef PIPE_ARCH_SSE

static void
store_row_8unorm_bgra_alphablend_premul_src_sse2(struct util_blit_sw_context *ctx,
                                                 void * restrict dst_row,
                                                 const uint32_t * restrict src_row,
                                                 uint32_t width)
{
   uint32_t cst_alpha = ctx->colororalpha;
   uint32_t x = 0;
   uint32_t * restrict dst32 = (uint32_t *)dst_row;
   __m128i dst, src;

   while ((((uintptr_t)dst32) & 0xf) && (x < width)) {
      blend_srcalpha_premul_pixel_8unorm(dst32, src_row[x], cst_alpha);
      dst32++;
      x++;
   }

   while (x + 4 <= width) {
      src = _mm_loadu_si128((const __m128i *)&src_row[x]);
      dst = _mm_load_si128((const __m128i *)dst32);
      dst = util_sse2_blend_premul_src_4(src, dst, cst_alpha);
      _mm_store_si128((__m128i *)dst32, dst);
      dst32 += 4;
      x += 4;
   }

   while (x < width) {
      blend_srcalpha_premul_pixel_8unorm(dst32, src_row[x], cst_alpha);
      dst32++;
      x++;
   }
}

#endif /* PIPE_ARCH_SSE */


/**
 * Store one row of bgra pixels with alpha blend using premultiplied src alpha.
 */
static void
store_row_8unorm_bgra_alphablend_premul_src(struct util_blit_sw_context *ctx,
                                            void * restrict dst_row,
                                            const uint32_t * restrict src_row,
                                            uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;
   uint32_t cst_alpha = ctx->colororalpha;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;
      blend_srcalpha_premul_pixel_8unorm(dst, src_pixel, cst_alpha);
      dst++;
   }
}


/**
 * Store one row of bgra pixels with color keying.
 */
static void
store_row_8unorm_bgra_colorkey(struct util_blit_sw_context *ctx,
                               void * restrict dst_row,
                               const uint32_t * restrict src_row,
                               uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;
   uint32_t color_key = ctx->colororalpha;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;
      if (src_pixel != color_key) {
         *dst = src_pixel;
      }
      dst++;
   }
}


/**
 * Store one row of bgra pixels with color keying, disregarding alpha value.
 */
static void
store_row_8unorm_bgra_colorkey_noalpha(struct util_blit_sw_context *ctx,
                                       void * restrict dst_row,
                                       const uint32_t * restrict src_row,
                                       uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;
   unsigned x;
   uint32_t color_key = ctx->colororalpha;

   for(x = 0; x < width; ++x) {
      uint32_t src_pixel = *src_row++;
      if ((src_pixel & 0x00FFFFFF) != color_key) {
         *dst = src_pixel;
      }
      dst++;
   }
}


/** Replace Z, preserve S */
static void
store_row_z_z24s8(struct util_blit_sw_context *ctx,
                  void * restrict dst_row,
                  const uint32_t * restrict src_row,
                  uint32_t width)
{
   uint32_t i, *dst = (uint32_t *) dst_row;
   for (i = 0; i < width; i++) {
      dst[i] = (src_row[i] & 0xffffff) | (dst[i] & 0xff000000);
   }
}


/** Replace Z, preserve S */
static void
store_row_z_s8z24(struct util_blit_sw_context *ctx,
                  void * restrict dst_row,
                  const uint32_t * restrict src_row,
                  uint32_t width)
{
   uint32_t i, *dst = (uint32_t *) dst_row;
   for (i = 0; i < width; i++) {
      dst[i] = (src_row[i] & 0xffffff00) | (dst[i] & 0xff);
   }
}


/** Replace Z, preserve S */
static void
store_row_z_z32s8x24(struct util_blit_sw_context *ctx,
                     void * restrict dst_row,
                     const uint32_t * restrict src_row,
                     uint32_t width)
{
   uint32_t i, *dst = (uint32_t *) dst_row;
   for (i = 0; i < width; i++) {
      dst[i*2+0] = src_row[i*2+0];
   }
}


/** Replace S, preserve Z */
static void
store_row_s_z24s8(struct util_blit_sw_context *ctx,
                  void * restrict dst_row,
                  const uint32_t * restrict src_row,
                  uint32_t width)
{
   uint32_t i, *dst = (uint32_t *) dst_row;
   for (i = 0; i < width; i++) {
      dst[i] = (src_row[i] & 0xff000000) | (dst[i] & 0xffffff);
   }
}


/** Replace S, preserve Z */
static void
store_row_s_s8z24(struct util_blit_sw_context *ctx,
                  void * restrict dst_row,
                  const uint32_t * restrict src_row,
                  uint32_t width)
{
   uint32_t i, *dst = (uint32_t *) dst_row;
   for (i = 0; i < width; i++) {
      dst[i] = (src_row[i] & 0xff) | (dst[i] & 0xffffff00);
   }
}


/** Replace S, preserve Z */
static void
store_row_s_z32s8x24(struct util_blit_sw_context *ctx,
                     void * restrict dst_row,
                     const uint32_t * restrict src_row,
                     uint32_t width)
{
   uint32_t i;
   uint8_t *dst = (uint8_t *) dst_row;
   const uint8_t *src = (const uint8_t *) src_row;
   for (i = 0; i < width; i++) {
      dst[i*8+4] = src[i*8+4];
   }
}


#ifdef PIPE_ARCH_SSE

/**
 * Fast SSE2 path for storing one row of PIPE_FORMAT_B8G8R8A8_UNORM pixels.
 */
static void
store_row_8unorm_bgra_sse2(struct util_blit_sw_context *ctx,
                           void * restrict dst_row,
                           const uint32_t * restrict src_row,
                           uint32_t width)
{
   uint32_t * restrict dst = (uint32_t *)dst_row;

   while (width && ((uintptr_t)dst & 0xf)) {
      uint32_t src_pixel = *src_row++;

      *dst++ =  (src_pixel & 0xff00ff00)
             | ((src_pixel & 0x000000ff) << 16)
             | ((src_pixel & 0x00ff0000) >> 16);
      --width;
   }

   while (width >= 4) {
      __m128i src_pixels;
      __m128i rg, b, a;
      __m128i dst_pixels;

      src_pixels = _mm_loadu_si128((const __m128i *)src_row);
      src_row += 4;

      rg =                _mm_and_si128(src_pixels, _mm_set1_epi32(0xff00ff00));
      b  = _mm_slli_epi32(_mm_and_si128(src_pixels, _mm_set1_epi32(0x000000ff)), 16);
      a  = _mm_srli_epi32(_mm_and_si128(src_pixels, _mm_set1_epi32(0x00ff0000)), 16);

      dst_pixels = _mm_or_si128(rg, _mm_or_si128(b, a));

#if USE_WRITE_COMBINING_STORES
      _mm_stream_si128((__m128i *)dst, dst_pixels);
#else
      _mm_store_si128((__m128i *)dst, dst_pixels);
#endif

      dst += 4;
      width -= 4;
   }

   while (width) {
      uint32_t src_pixel = *src_row++;

      *dst++ =  (src_pixel & 0xff00ff00)
             | ((src_pixel & 0x000000ff) << 16)
             | ((src_pixel & 0x00ff0000) >> 16);
      --width;
   }

#if USE_WRITE_COMBINING_STORES
   _mm_sfence();
#endif
}

#endif /* PIPE_ARCH_SSE */


static void
nearest_stretch_rows_8unorm(struct util_blit_sw_context *ctx)
{
   const struct util_format_description *dst_desc = ctx->dst_desc;
   void *dst = ctx->dst;
   uint32_t dst_stride = ctx->dst_stride;

   int32_t dst_x0 = ctx->dst_x0;
   int32_t dst_y0 = ctx->dst_y0;
   int32_t dst_y1 = ctx->dst_y1;

   int32_t dst_delta_x = ctx->dst_delta_x;

   uint32_t *stretched_row;

   int32_t src_y_fp = ctx->src_y_fp;
   int32_t dst_y;

   for (dst_y = dst_y0; dst_y < dst_y1; ++dst_y) {
      int32_t src_y = (src_y_fp + 0x8000) >> 16;
      uint8_t *dst_row;

      src_y = CLAMP(src_y, 0, ctx->clamp_y);

      dst_row = (uint8_t *)dst
              + dst_stride*dst_y
              + (dst_x0/dst_desc->block.width)*(dst_desc->block.bits/8);

      stretched_row = fetch_and_stretch_row_8unorm(ctx, src_y);

      ctx->store_row_8unorm(ctx, dst_row, stretched_row, dst_delta_x);

      src_y_fp += ctx->src_ystep_fp;
   }
}


static void
linear_stretch_rows_8unorm(struct util_blit_sw_context *ctx)
{
   const struct util_format_description *dst_desc = ctx->dst_desc;
   void *dst = ctx->dst;
   uint32_t dst_stride = ctx->dst_stride;

   int32_t dst_x0 = ctx->dst_x0;
   int32_t dst_y0 = ctx->dst_y0;
   int32_t dst_y1 = ctx->dst_y1;

   int32_t dst_delta_x = ctx->dst_delta_x;

   uint32_t *stretched_row0;
   uint32_t *stretched_row1;
   uint32_t *lerped_row = ctx->store_row;

   int32_t src_y_fp = ctx->src_y_fp;
   int32_t dst_y;

   for (dst_y = dst_y0; dst_y < dst_y1; ++dst_y) {
      int32_t src_y0 = src_y_fp >> 16;
      uint32_t weight_y = (src_y_fp >> 8) & 0xff;
      uint8_t *dst_row;

      /*
       * Clamp against src top/bottom edges.
       */

      if (src_y0 < 0) {
         src_y0 = 0;
         weight_y = 0;
      }

      if (src_y0 >= ctx->clamp_y) {
         src_y0 = ctx->clamp_y;
         weight_y = 0;
      }

      dst_row = (uint8_t *)dst
              + dst_stride*dst_y
              + (dst_x0/dst_desc->block.width)*(dst_desc->block.bits/8);

      stretched_row0 = fetch_and_stretch_row_8unorm(ctx, src_y0);

      if (weight_y) {
         int32_t src_y1 = src_y0 + 1;
         stretched_row1 = fetch_and_stretch_row_8unorm(ctx, src_y1);

         ctx->lerp_row_8unorm(lerped_row,
                              stretched_row0,
                              stretched_row1,
                              weight_y,
                              dst_delta_x);

         ctx->store_row_8unorm(ctx, dst_row, lerped_row, dst_delta_x);
      } else {
         ctx->store_row_8unorm(ctx, dst_row, stretched_row0, dst_delta_x);
      }

      src_y_fp += ctx->src_ystep_fp;
   }
}


/**
 * Blit between two surfaces in memory.
 *
 * The source texels coordinates will be clamped to [0, src_width - 1]  and
 * [0, src_height - 1] for x and y respectively.
 */
static void
stretch_blit_8unorm(struct util_blit_sw_context *ctx,
                    const struct util_format_description *src_desc,
                    const void * restrict src, uint32_t src_stride,
                    int32_t src_width, int32_t src_height,
                    int32_t src_x0, int32_t src_y0,
                    int32_t src_x1, int32_t src_y1,
                    const void * restrict src2,
                    const struct util_format_description *dst_desc,
                    void * restrict dst, uint32_t dst_stride,
                    int32_t dst_x0, int32_t dst_y0,
                    int32_t dst_x1, int32_t dst_y1,
                    int32_t clip_x0, int32_t clip_y0,
                    int32_t clip_x1, int32_t clip_y1,
                    unsigned filter,
                    uint32_t rop_op,
                    uint32_t colororalphaorlogicop,
                    uint32_t flags,
                    unsigned writemask)
{
   int32_t src_xmin;
   int32_t src_xmax;
   int32_t fetch_xmin;
   int32_t fetch_xmax;
   int32_t src_delta_x;
   int32_t src_delta_y;
   int32_t clamp_src_x0;
   int32_t clamp_src_x1;
   int32_t clamp_dst_x0;
   int32_t clamp_dst_x1;
   uint32_t block_width;

   assert(src_desc->block.height == 1);
   assert(dst_desc->block.height == 1);
   if (src_desc->block.height != 1 ||
       dst_desc->block.height != 1) {
      return;
   }

   /*
    * Setup the context.
    */
   ctx->writemask = writemask;

   ctx->src_desc = src_desc;
   ctx->src = src;
   ctx->src2 = src2;
   ctx->src_stride = src_stride;
   ctx->dst_desc = dst_desc;
   ctx->dst = dst;
   ctx->dst_stride = dst_stride;
   ctx->dst_x0 = dst_x0;
   ctx->dst_y0 = dst_y0;
   ctx->dst_x1 = dst_x1;
   ctx->dst_y1 = dst_y1;

   ctx->dst_delta_x = dst_x1 - dst_x0;
   ctx->dst_delta_y = dst_y1 - dst_y0;
   src_delta_x = src_x1 - src_x0;
   src_delta_y = src_y1 - src_y0;

   ctx->clamp_y = src_height - 1;

   /*
    * Determine how many pixels to fetch.
    */

   if (src_delta_x >= 0) {
      src_xmin = src_x0;
      src_xmax = src_x1;
   } else {
      src_xmin = src_x1;
      src_xmax = src_x0;
   }

   // XXX in theory should clamp against src_width-1...
   fetch_xmin = CLAMP(src_xmin, 0, src_width);
   fetch_xmax = CLAMP(src_xmax, 0, src_width);

   block_width = src2 ? 2 : src_desc->block.width;

   fetch_xmin = round_down(fetch_xmin, block_width);
   fetch_xmax = round_up  (fetch_xmax, block_width);

   ctx->fetch_width = fetch_xmax - fetch_xmin;

   assert(     fetch_xmin     >= 0);
   assert(ctx->fetch_width    >= 1);
   assert(     fetch_xmin     % block_width == 0);
   assert(ctx->fetch_width    % block_width == 0);

   /*
    * Note for nv12, for y plane need src_desc->block_width (1) here (not 2).
    */
   ctx->src = (const uint8_t *)ctx->src +
      (fetch_xmin/src_desc->block.width) * (src_desc->block.bits/8);
   if (ctx->src2) {
      ctx->src2 = (const uint8_t *)ctx->src2 + (fetch_xmin/2) * 2;
   }   
   src_x0 -= fetch_xmin;
   src_x1 -= fetch_xmin;

   /*
    * Compute fixed point interpolation coefficients.
    */

   ctx->src_x_fp = src_x0 << 16;
   ctx->src_y_fp = src_y0 << 16;

   ctx->src_xstep_fp = (src_delta_x << 16)/ctx->dst_delta_x;
   ctx->src_ystep_fp = (src_delta_y << 16)/ctx->dst_delta_y;

   ctx->src_x_fp += (ctx->src_xstep_fp >> 1) - 0x8000;
   ctx->src_y_fp += (ctx->src_ystep_fp >> 1) - 0x8000;

   if (dst_x0 != clip_x0 ||
       dst_x1 != clip_x1 ||
       dst_y0 != clip_y0 ||
       dst_y1 != clip_y1) {

      /*
       * re-calculate the values as appropriate (essentially perform the iterations
       * up to hitting the clip rect).
       */
      int32_t incr_fp0;
      int32_t incr_fp1;
      int32_t src_x1_fp;

      assert(ctx->src2 == NULL);
      ctx->dst_x0 = clip_x0;
      ctx->dst_x1 = clip_x1;
      ctx->dst_y0 = clip_y0;
      ctx->dst_y1 = clip_y1;
      ctx->dst_delta_x = clip_x1 - clip_x0;
      ctx->dst_delta_y = clip_y1 - clip_y0;

      /*
       * Need to make sure src_xstep_fp isn't 0, as that would lead to div by
       * zero. (Can happen due to dst coords being virtually unbounded, scaling
       * factor in excess of 65536.)
       * Just round to -1/1, instead.
       */
      if (ctx->src_xstep_fp == 0) {
         if (src_delta_x >= 0) {
            ctx->src_xstep_fp = 1;
         } else {
            ctx->src_xstep_fp = -1;
         }
      }
      incr_fp0 = (clip_x0 - dst_x0) * ctx->src_xstep_fp;
      incr_fp1 = (clip_x1 - dst_x0) * ctx->src_xstep_fp;
      src_x1_fp = ctx->src_x_fp + incr_fp1;
      ctx->src_x_fp += incr_fp0;

      ctx->src_y_fp += (clip_y0 - dst_y0) * ctx->src_ystep_fp;

      /*
       * Determine which values we really need to fetch, and adjust
       * things accordingly.
       */
      if (src_delta_x >= 0) {
         /*
          * Actual (nearest) fetch will add 0x8000.
          * We'll add 0x10000 so it would also work for linear
          * filtering (without clamping when it's not necessary),
          * and this should also help with fetch_width required
          * to be at least 1...
          */
         src_xmin = ctx->src_x_fp;
         src_xmax = src_x1_fp + 0x10000;
      } else {
         src_xmin = src_x1_fp;
         src_xmax = ctx->src_x_fp + 0x10000;
      }
      src_xmin = CLAMP(src_xmin >> 16, 0, src_width - 1);
      src_xmax = CLAMP(src_xmax >> 16, 0, src_width - 1);
      ctx->src_x_fp -= src_xmin << 16;

      /*
       * Fetch width must be at least 1, it is possible both src_xmin/xmax
       * point at the same value (at the edges, 0 or src_width-1).
       */
      ctx->fetch_width = MAX2(src_xmax - src_xmin, 1);

      ctx->src = (const uint8_t *)ctx->src + src_xmin * (src_desc->block.bits/8);
   }

   /*
    * Determine where clamping needs to happen.
    */

   if (src_delta_x >= 0) {
      clamp_src_x0 = 0;
      clamp_src_x1 = ctx->fetch_width - 1;
   }
   else {
      clamp_src_x0 = ctx->fetch_width - 1;
      clamp_src_x1 = 0;
   }
   assert(ctx->src_xstep_fp != 0);
   clamp_dst_x0 = ((clamp_src_x0 << 16) - ctx->src_x_fp + ctx->src_xstep_fp - 1) / ctx->src_xstep_fp;
   clamp_dst_x1 = ((clamp_src_x1 << 16) - ctx->src_x_fp) / ctx->src_xstep_fp;

   assert(clamp_dst_x1 >= clamp_dst_x0 || clamp_src_x0 == clamp_src_x1);

   assert(ctx->src_x_fp + clamp_dst_x0 * ctx->src_xstep_fp >= 0);
   assert(ctx->src_x_fp + clamp_dst_x0 * ctx->src_xstep_fp <= ctx->fetch_width << 16);
   if (clamp_dst_x1 >= clamp_dst_x0) {
      assert(ctx->src_x_fp + clamp_dst_x1 * ctx->src_xstep_fp >= 0);
      assert(ctx->src_x_fp + clamp_dst_x1 * ctx->src_xstep_fp <= ctx->fetch_width << 16);
   }

   clamp_dst_x0 = CLAMP(clamp_dst_x0, 0, ctx->dst_delta_x);
   clamp_dst_x1 = CLAMP(clamp_dst_x1, 0, ctx->dst_delta_x);

   ctx->clamp_src_x0 = clamp_src_x0;
   ctx->clamp_src_x1 = clamp_src_x1;
   ctx->clamp_dst_x0 = clamp_dst_x0;
   ctx->clamp_dst_x1 = clamp_dst_x1;

   if (0) {
      debug_printf("src_format   = %s\n", src_desc->short_name);
      debug_printf("dst_format   = %s\n", dst_desc->short_name);
      debug_printf("src_width    = %i\n", src_width  );
      debug_printf("src_height   = %i\n", src_height );
      debug_printf("fetch_xmin   = %i\n", fetch_xmin);
      debug_printf("fetch_width  = %i\n", ctx->fetch_width);
      debug_printf("src_delta_x  = %i\n", src_delta_x);
      debug_printf("src_delta_y  = %i\n", src_delta_y);
      debug_printf("dst_delta_x  = %i\n", ctx->dst_delta_x);
      debug_printf("dst_delta_y  = %i\n", ctx->dst_delta_y);
      debug_printf("src_x_fp     = %f\n", ctx->src_x_fp / 65536.0);
      debug_printf("src_y_fp     = %f\n", ctx->src_y_fp / 65536.0);
      debug_printf("src_xstep_fp = %f\n", ctx->src_xstep_fp / 65536.0);
      debug_printf("src_ystep_fp = %f\n", ctx->src_ystep_fp / 65536.0);
      debug_printf("clamp_src_x0 = %i\n", ctx->clamp_src_x0);
      debug_printf("clamp_src_x1 = %i\n", ctx->clamp_src_x1);
      debug_printf("clamp_dst_x0 = %i\n", ctx->clamp_dst_x0);
      debug_printf("clamp_dst_x1 = %i\n", ctx->clamp_dst_x1);
      debug_printf("\n");
   }

   ctx->fetched_row_y = -1;
   ctx->stretched_row_y[0] = -1;
   ctx->stretched_row_y[1] = -1;
   ctx->stretched_row_index = 0;

   /*
    * XXX we should probably take advantage of no-swizzle path
    * (fetch_row_memcpy/store_row_memcpy) in some way or another
    * for ordinary bgra8->bgra8 blits too.
    */

   /*
    * Choose the 'fetch' function
    */
   if (writemask & PIPE_MASK_ZS) {
      assert(src_desc->format == dst_desc->format);
      ctx->fetch_row_8unorm = fetch_row_memcpy;
   }
   else {
      ctx->fetch_row_8unorm = &fetch_row_8unorm;
      if (src2) {
         ctx->fetch_row_8unorm = &fetch_row_8unorm_nv12;
      }
#ifdef PIPE_ARCH_SSE
      if (util_cpu_caps.has_sse2) {
         if (src_desc->format == PIPE_FORMAT_YUYV) {
            ctx->fetch_row_8unorm = &fetch_row_8unorm_yuyv_sse2;
         }
         else if (src_desc->format == PIPE_FORMAT_UYVY) {
            ctx->fetch_row_8unorm = &fetch_row_8unorm_uyvy_sse2;
         }
         else if (src2) {
            ctx->fetch_row_8unorm = &fetch_row_8unorm_nv12_sse2;
         }
      }
#endif /* PIPE_ARCH_SSE */
   }

   /*
    * Choose the 'stretch' function
    */
   if (filter == PIPE_TEX_FILTER_LINEAR) {
      ctx->stretch_row_8unorm = &linear_stretch_row_8unorm;
   } else {
      if (writemask & PIPE_MASK_RGBA) {
         ctx->stretch_row_8unorm = &nearest_stretch_row_8unorm;
      }
      else {
         if (src_desc->block.bits == 16) {
            ctx->stretch_row_8unorm = &nearest_stretch_row_16;
         }
         else if (src_desc->block.bits == 64) {
            ctx->stretch_row_8unorm = &nearest_stretch_row_64;
         }
         else {
            assert(src_desc->block.bits == 32);
            ctx->stretch_row_8unorm = &nearest_stretch_row_8unorm;
         }
      }
   }

   /*
    * Choose the 'lerp' function
    */
   ctx->lerp_row_8unorm = &lerp_row_8unorm;
#ifdef PIPE_ARCH_SSE
   if (util_cpu_caps.has_sse2) {
      ctx->lerp_row_8unorm = &lerp_row_8unorm_sse2;
   }
#endif /* PIPE_ARCH_SSE */

   /*
    * Choose the 'store' function
    */
   if (writemask & PIPE_MASK_ZS) {
      if (ctx->writemask == PIPE_MASK_ZS) {
         ctx->store_row_8unorm = store_row_memcpy;
      }
      else if (ctx->writemask == PIPE_MASK_Z) {
         switch (ctx->src_desc->format) {
         case PIPE_FORMAT_Z24_UNORM_S8_UINT:
            ctx->store_row_8unorm = store_row_z_z24s8;
            break;
         case PIPE_FORMAT_S8_UINT_Z24_UNORM:
            ctx->store_row_8unorm = store_row_z_s8z24;
            break;
         case PIPE_FORMAT_Z32_FLOAT_S8X24_UINT:
            ctx->store_row_8unorm = store_row_z_z32s8x24;
            break;
         case PIPE_FORMAT_Z24X8_UNORM:
         case PIPE_FORMAT_X8Z24_UNORM:
         default:
            ctx->store_row_8unorm = store_row_memcpy;
         }
      }
      else {
         assert(ctx->writemask == PIPE_MASK_S);
         switch (ctx->src_desc->format) {
         case PIPE_FORMAT_Z24_UNORM_S8_UINT:
            ctx->store_row_8unorm = store_row_s_z24s8;
            break;
         case PIPE_FORMAT_S8_UINT_Z24_UNORM:
            ctx->store_row_8unorm = store_row_s_s8z24;
            break;
         case PIPE_FORMAT_Z32_FLOAT_S8X24_UINT:
            ctx->store_row_8unorm = store_row_s_z32s8x24;
            break;
         case PIPE_FORMAT_Z24X8_UNORM:
         case PIPE_FORMAT_X8Z24_UNORM:
         default:
            ctx->store_row_8unorm = store_row_memcpy;
         }
      }
   }
   else {
      ctx->store_row_8unorm = &store_row_8unorm;
      if (dst_desc->format == PIPE_FORMAT_B8G8R8A8_UNORM ||
          dst_desc->format == PIPE_FORMAT_B8G8R8X8_UNORM) {
         ctx->store_row_8unorm = &store_row_8unorm_bgra;
#ifdef PIPE_ARCH_SSE
         if (util_cpu_caps.has_sse2) {
            ctx->store_row_8unorm = &store_row_8unorm_bgra_sse2;
         }
#endif /* PIPE_ARCH_SSE */
      }
   }

   /*
    * XXX we integrate the alphablend/colorkeying/logicop into store.
    * Unsure if that's the best thing to do. Could fetch another src instead.
    */
   if (rop_op) {
      assert((writemask & PIPE_MASK_ZS) == 0);
      if ((src_desc->format == PIPE_FORMAT_B8G8R8A8_UNORM)) {
         ctx->fetch_row_8unorm = &fetch_row_memcpy;
      }
      else {
         assert(src_desc->format == PIPE_FORMAT_B8G8R8X8_UNORM);
         ctx->fetch_row_8unorm = &fetch_row_bgrx_noswizzle;
      }
      if (rop_op == UTIL_BLIT_SW_ROP_OP_COLOR_KEYING) {
         ctx->colororalpha = colororalphaorlogicop;
         if (flags & UTIL_BLIT_SW_ROP_FLAG_USESRCALPHA) {
            ctx->store_row_8unorm = &store_row_8unorm_bgra_colorkey;
         }
         else {
            ctx->store_row_8unorm = &store_row_8unorm_bgra_colorkey_noalpha;
         }
      }
      else if (rop_op == UTIL_BLIT_SW_ROP_OP_LOGICOP) {
         switch (colororalphaorlogicop) {
         case PIPE_LOGICOP_CLEAR:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_clear;
            break;
         case PIPE_LOGICOP_NOR:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_nor;
            break;
         case PIPE_LOGICOP_AND_INVERTED:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_and_inverted;
            break;
         case PIPE_LOGICOP_COPY_INVERTED:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_copy_inverted;
            break;
         case PIPE_LOGICOP_AND_REVERSE:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_and_reverse;
            break;
         case PIPE_LOGICOP_INVERT:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_invert;
            break;
         case PIPE_LOGICOP_XOR:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_xor;
            break;
         case PIPE_LOGICOP_NAND:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_nand;
            break;
         case PIPE_LOGICOP_AND:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_and;
            break;
         case PIPE_LOGICOP_EQUIV:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_equiv;
            break;
         case PIPE_LOGICOP_NOOP:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_noop;
            break;
         case PIPE_LOGICOP_OR_INVERTED:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_or_inverted;
            break;
         case PIPE_LOGICOP_COPY:
            ctx->store_row_8unorm = &store_row_memcpy;
            break;
         case PIPE_LOGICOP_OR_REVERSE:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_or_reverse;
            break;
         case PIPE_LOGICOP_OR:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_or;
            break;
         case PIPE_LOGICOP_SET:
            ctx->store_row_8unorm = &store_row_8unorm_bgra_set;
            break;
         default:
            assert(0);
            break;
         }
      }
      else {
         assert(rop_op == UTIL_BLIT_SW_ROP_OP_ALPHABLEND);
         /*
          * Not quite as accurate as possible but good enough
          * for gditest_hw with the -tolerance option.
          */
         if ((flags & UTIL_BLIT_SW_ROP_FLAG_USESRCALPHA) &&
             (colororalphaorlogicop == 0xff)) {
#ifdef PIPE_ARCH_SSE
            if (util_cpu_caps.has_sse2) {
               ctx->store_row_8unorm = &store_row_8unorm_bgra_alphablend_src_sse2;
            } else
#endif /* PIPE_ARCH_SSE */
            ctx->store_row_8unorm = &store_row_8unorm_bgra_alphablend_src;
         }
         else if (flags & UTIL_BLIT_SW_ROP_FLAG_USESRCALPHA) {
            ctx->colororalpha = colororalphaorlogicop;
#ifdef PIPE_ARCH_SSE
            if (util_cpu_caps.has_sse2) {
               ctx->store_row_8unorm = &store_row_8unorm_bgra_alphablend_premul_src_sse2;
            } else
#endif /* PIPE_ARCH_SSE */
            ctx->store_row_8unorm = &store_row_8unorm_bgra_alphablend_premul_src;
         }
         else {
            ctx->colororalpha = colororalphaorlogicop;
#ifdef PIPE_ARCH_SSE
            if (util_cpu_caps.has_sse2) {
               ctx->store_row_8unorm = &store_row_8unorm_bgra_alphablend_cst_sse2;
            } else
#endif /* PIPE_ARCH_SSE */
            ctx->store_row_8unorm = &store_row_8unorm_bgra_alphablend_cst;
         }
      }
   }

   if (filter == PIPE_TEX_FILTER_LINEAR) {
      linear_stretch_rows_8unorm(ctx);
   } else {
      nearest_stretch_rows_8unorm(ctx);
   }
}


/**
 * Do software-based blit.
 * \param filter  one of PIPE_TEX_FILTER_NEAREST/LINEAR
 * \param writemask  bitmask of PIPE_MASK_[RGBAZS] flags (not fully supported!!)
 */
boolean
util_blit_sw_resource(struct util_blit_sw_context *ctx,
                      struct pipe_context *pipe,
                      struct pipe_resource *src_res,
                      enum pipe_format src_format,
                      unsigned src_level,
                      int32_t src_x0, int32_t src_y0,
                      int32_t src_x1, int32_t src_y1,
                      uint32_t src_z,
                      struct pipe_resource *dst_res,
                      enum pipe_format dst_format,
                      unsigned dst_level,
                      int32_t dst_x0, int32_t dst_y0,
                      int32_t dst_x1, int32_t dst_y1,
                      uint32_t dst_z,
                      unsigned filter,
                      unsigned writemask)
{
   const struct util_format_description *src_desc;
   const struct util_format_description *dst_desc;
   unsigned src_width  = u_minify(src_res->width0,  src_level);
   unsigned src_height = u_minify(src_res->height0, src_level);
   unsigned dst_width  = u_minify(dst_res->width0,  dst_level);
   unsigned dst_height = u_minify(dst_res->height0, dst_level);
   int32_t src_xmin, src_xmax, src_ymin, src_ymax;
   int32_t dst_xmin, dst_xmax, dst_ymin, dst_ymax;
   struct pipe_transfer *src_transf;
   struct pipe_transfer *src_transf2 = NULL;
   struct pipe_transfer *dst_transf;
   const uint8_t *src_mem;
   const uint8_t *src_mem2 = NULL;
   uint8_t *dst_mem;
   boolean ret = FALSE;
   boolean is_nv12_blit = FALSE;
   enum pipe_transfer_usage access;
   unsigned src_block_width;
   unsigned src_block_height;

   /* Writemasks for color not supported at this time */
   if ((writemask & PIPE_MASK_RGBA) != 0 &&
       (writemask & PIPE_MASK_RGBA) != PIPE_MASK_RGBA) {
      return FALSE;
   }

   /*
    * Check for consistent geometry
    */

   assert(dst_level <= dst_res->last_level);
   assert(src_level <= src_res->last_level);

   /*
    * Reorder vertices such that
    *
    *    dst_x0 <= dst_x1
    *    dst_y0 <= dst_y1
    */

   if (dst_x0 > dst_x1) {
      int32_t tmp;

      tmp = dst_x0;
      dst_x0 = dst_x1;
      dst_x1 = tmp;

      tmp = src_x0;
      src_x0 = src_x1;
      src_x1 = tmp;
   }

   if (dst_y0 > dst_y1) {
      int32_t tmp;

      tmp = dst_y0;
      dst_y0 = dst_y1;
      dst_y1 = tmp;

      tmp = src_y0;
      src_y0 = src_y1;
      src_y1 = tmp;
   }

   /*
    * Skip null/clipped rectangles.
    */

   if (dst_x0 >= dst_width || dst_x1 <= 0 ||
       dst_y0 >= dst_height || dst_y1 <= 0) {
      return TRUE;
   }

   src_desc = util_format_description(src_format);
   dst_desc = util_format_description(dst_format);

   src_block_width = src_desc->block.width;
   src_block_height = src_desc->block.height;

   /*
    * XXX: do we need explicit flag for nv12 blit? */
   if (src_res->format == PIPE_FORMAT_R8_UNORM &&
       src_res->next) {
      is_nv12_blit = TRUE;
      assert(src_res->next->format == PIPE_FORMAT_R8G8_UNORM);
      /*
       * Need to make sure we always map blocks aligned to width/height of 2
       * (otherwise chroma value addresses won't match the luma addresses).
       */
      src_block_width = 2;
      src_block_height = 2;
   }

   if (writemask & PIPE_MASK_ZS) {
      if (src_desc->block.bits > 32 &&
          (src_width >= MAX_SURFACE_WIDTH / 2 ||
           dst_width >= MAX_SURFACE_WIDTH / 2)) {
         /* Our temporary row buffers hold MAX_SURFACE_WIDTH 32-bit pixels.
          * Bail out for too-wide 64-bit blits for now.
          */
         debug_warning("SW blit size too wide");
         return FALSE;
      }
      if (src_format != dst_format) {
         debug_warning("mixed-format Z/S blits not supported");
         return FALSE;
      }
   }
   else {
      /*
       * Restrictions on src and dst formats since intermediate results
       * are unorm8 (and we don't handle compressed formats, but do
       * handle yuv->rgba8).
       */
      if (!((util_format_fits_8unorm(src_desc) ||
           util_format_fits_8unorm(dst_desc)) &&
          src_desc->unpack_rgba_8unorm &&
          src_desc->block.height == 1 &&
          dst_desc->pack_rgba_8unorm &&
          dst_desc->block.width == 1 &&
          dst_desc->block.height == 1)) {
         return FALSE;
      }
   }

   /*
    * Clip destination rectangle
    */

   if (dst_x0 < 0) {
      src_x0 = src_x0 + (src_x1 - src_x0)*(0 - dst_x0)/(dst_x1 - dst_x0);
      dst_x0 = 0;
   }

   if (dst_x1 > dst_width) {
      src_x1 = src_x0 + (src_x1 - src_x0)*(dst_width - dst_x0)/(dst_x1 - dst_x0);
      dst_x1 = dst_width;
   }

   if (dst_y0 < 0) {
      src_y0 = src_y0 + (src_y1 - src_y0)*(0 - dst_y0)/(dst_y1 - dst_y0);
      dst_y0 = 0;
   }

   if (dst_y1 > dst_height) {
      src_y1 = src_y0 + (src_y1 - src_y0)*(dst_height - dst_y0)/(dst_y1 - dst_y0);
      dst_y1 = dst_height;
   }

   /*
    * Round rects to the outer pixel block boundaries
    */

   if (src_x0 <= src_x1) {
      src_xmin = src_x0;
      src_xmax = src_x1;
   } else {
      src_xmin = src_x1;
      src_xmax = src_x0;
   }

   if (src_y0 <= src_y1) {
      src_ymin = src_y0;
      src_ymax = src_y1;
   } else {
      src_ymin = src_y1;
      src_ymax = src_y0;
   }

   src_xmin = round_down(src_xmin, src_block_width);
   src_xmax = round_up  (src_xmax, src_block_width);
   src_ymin = round_down(src_ymin, src_block_height);
   src_ymax = round_up  (src_ymax, src_block_height);

   src_xmin = MAX2(src_xmin, 0);
   src_xmax = MIN2(src_xmax, src_width);
   src_ymin = MAX2(src_ymin, 0);
   src_ymax = MIN2(src_ymax, src_height);

   assert(dst_x0 <= dst_x1);
   assert(dst_y0 <= dst_y1);

   dst_xmin = round_down(dst_x0, dst_desc->block.width);
   dst_xmax = round_up  (dst_x1, dst_desc->block.width);
   dst_ymin = round_down(dst_y0, dst_desc->block.height);
   dst_ymax = round_up  (dst_y1, dst_desc->block.height);

   assert(dst_xmin >= 0);
   assert(dst_ymin >= 0);

   dst_xmax = MIN2(dst_xmax, dst_width);
   dst_ymax = MIN2(dst_ymax, dst_height);

   src_width  = src_xmax - src_xmin;
   src_height = src_ymax - src_ymin;

   dst_width  = dst_xmax - dst_xmin;
   dst_height = dst_ymax - dst_ymin;

   assert(src_width > 0);
   assert(dst_width > 0);

   /*
    * Transfer the bounding boxes.
    */

   src_mem = pipe_transfer_map(pipe, src_res,
                               src_level, src_z,
                               PIPE_TRANSFER_READ,
                               src_xmin, src_ymin,
                               src_width, src_height,
                               &src_transf);
   if (!src_mem) {
      goto no_src_transf;
   }

   if (is_nv12_blit) {
      src_mem2 = pipe_transfer_map(pipe, src_res->next,
                                   src_level, src_z,
                                   PIPE_TRANSFER_READ,
                                   src_xmin / 2, src_ymin / 2,
                                   (src_width + 1) / 2, (src_height + 1) / 2,
                                   &src_transf2);

      if (!src_mem2) {
         goto no_dst_transf;
      }
   }

   if (util_format_is_depth_and_stencil(dst_format)
       && (writemask != PIPE_MASK_ZS))
      access = PIPE_TRANSFER_READ_WRITE;
   else
      access = PIPE_TRANSFER_WRITE;

   dst_mem = pipe_transfer_map(pipe, dst_res,
                               dst_level, dst_z,
                               access,
                               dst_xmin, dst_ymin,
                               dst_width, dst_height,
                               &dst_transf);
   if (!dst_mem) {
      goto no_dst_transf;
   }

   /*
    * Compute the rectangles relative to the bounding boxes
    */

   src_x0 -= src_xmin;
   src_x1 -= src_xmin;
   src_y0 -= src_ymin;
   src_y1 -= src_ymin;

   dst_x0 -= dst_xmin;
   dst_x1 -= dst_xmin;
   dst_y0 -= dst_ymin;
   dst_y1 -= dst_ymin;

   /*
    * Blit
    */

   stretch_blit_8unorm(ctx,
                       src_desc,
                       src_mem,  src_transf->stride,
                       src_width, src_height,
                       src_x0, src_y0,
                       src_x1, src_y1,
                       src_mem2,
                       dst_desc,
                       dst_mem, dst_transf->stride,
                       dst_x0,  dst_y0,
                       dst_x1,  dst_y1,
                       dst_x0,  dst_y0,
                       dst_x1,  dst_y1,
                       filter, 0, 0, 0, writemask);

   ret = TRUE;

   pipe->transfer_unmap(pipe, dst_transf);
no_dst_transf:
   if (src_transf2)
      pipe->transfer_unmap(pipe, src_transf2);
   pipe->transfer_unmap(pipe, src_transf);
no_src_transf:
   return ret;
}


void
util_blit_8unorm_rop_sw(struct util_blit_sw_context *ctx,
                        struct pipe_context *pipe,
                        struct pipe_resource *src_res,
                        int32_t src_x0, int32_t src_y0,
                        int32_t src_x1, int32_t src_y1,
                        struct pipe_resource *dst_res,
                        int32_t dst_x0, int32_t dst_y0,
                        int32_t dst_x1, int32_t dst_y1,
                        int32_t clip_x0, int32_t clip_y0,
                        int32_t clip_x1, int32_t clip_y1,
                        uint32_t rop_op,
                        uint32_t colororalphaorlogicop,
                        uint32_t flags)
{
   const struct util_format_description *src_desc, *dst_desc;
   struct pipe_transfer *src_transf, *dst_transf;
   const uint8_t *src_mem;
   uint8_t *dst_mem;
   int32_t src_xmin, src_xmax, src_ymin, src_ymax;
   int32_t src_width, src_height;
   int32_t clip_width, clip_height;

   if ((src_res->format != PIPE_FORMAT_B8G8R8A8_UNORM &&
        src_res->format != PIPE_FORMAT_B8G8R8X8_UNORM) ||
       (dst_res->format != PIPE_FORMAT_B8G8R8A8_UNORM &&
        dst_res->format != PIPE_FORMAT_B8G8R8X8_UNORM)) {
      debug_printf("unhandled color format src %d dst %d\n",
                   src_res->format, dst_res->format);
      return;
   }

   /*
    * src, clip parameters never exceed surface bounds (but src can be
    * swapped). dst can exceed surface bounds (in all directions).
    */

   if (src_x0 <= src_x1) {
      src_xmin = src_x0;
      src_xmax = src_x1;
   } else {
      src_xmin = src_x1;
      src_xmax = src_x0;
   }

   if (src_y0 <= src_y1) {
      src_ymin = src_y0;
      src_ymax = src_y1;
   } else {
      src_ymin = src_y1;
      src_ymax = src_y0;
   }

   src_width  = src_xmax - src_xmin;
   src_height = src_ymax - src_ymin;

   clip_width  = clip_x1 - clip_x0;
   clip_height = clip_y1 - clip_y0;

   src_desc = util_format_description(src_res->format);
   dst_desc = util_format_description(dst_res->format);

   /*
    * Transfer the bounding boxes.
    */

   src_mem = pipe_transfer_map(pipe, src_res,
                               0, 0,
                               PIPE_TRANSFER_READ,
                               src_xmin, src_ymin,
                               src_width, src_height,
                               &src_transf);
   if (!src_mem) {
      goto no_src_transf;
   }

   dst_mem = pipe_transfer_map(pipe, dst_res,
                               0, 0,
                               PIPE_TRANSFER_READ_WRITE,
                               clip_x0, clip_y0,
                               clip_width, clip_height,
                               &dst_transf);
   if (!dst_mem) {
      goto no_dst_transf;
   }

   src_x0 -= src_xmin;
   src_x1 -= src_xmin;
   src_y0 -= src_ymin;
   src_y1 -= src_ymin;
   /* all coords are relative to mapping */
   dst_x0 = dst_x0 - clip_x0;
   dst_x1 = dst_x1 - clip_x0;
   dst_y0 = dst_y0 - clip_y0;
   dst_y1 = dst_y1 - clip_y0;
   clip_x0 = 0;
   clip_x1 = clip_width;
   clip_y0 = 0;
   clip_y1 = clip_height;

   stretch_blit_8unorm(ctx,
                       src_desc,
                       src_mem,  src_transf->stride,
                       src_width, src_height,
                       src_x0, src_y0,
                       src_x1, src_y1,
                       NULL,
                       dst_desc,
                       dst_mem, dst_transf->stride,
                       dst_x0,  dst_y0,
                       dst_x1,  dst_y1,
                       clip_x0, clip_y0,
                       clip_x1, clip_y1,
                       PIPE_TEX_FILTER_NEAREST,
                       rop_op, colororalphaorlogicop,
                       flags, PIPE_MASK_RGBA);

   pipe->transfer_unmap(pipe, dst_transf);
no_dst_transf:
   pipe->transfer_unmap(pipe, src_transf);
no_src_transf:
   return;
}

void
util_blit_sw_destroy(struct util_blit_sw_context *ctx)
{
   align_free(ctx);
}


struct util_blit_sw_context *
util_blit_sw_create(void)
{
   return align_malloc(sizeof(struct util_blit_sw_context), 16);
}





