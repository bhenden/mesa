/**************************************************************************
 *
 * Copyright 2020 VMware, Inc.
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 **************************************************************************/


#ifndef U_BLIT_SW_H_
#define U_BLIT_SW_H_


#include "pipe/p_compiler.h"
#include "pipe/p_format.h"
#include "pipe/p_state.h"

#define UTIL_BLIT_SW_ROP_OP_COPY 0
#define UTIL_BLIT_SW_ROP_OP_COLOR_KEYING 1
#define UTIL_BLIT_SW_ROP_OP_LOGICOP 2
#define UTIL_BLIT_SW_ROP_OP_ALPHABLEND 3

#define UTIL_BLIT_SW_ROP_FLAG_USESRCALPHA 1


struct pipe_context;

struct util_blit_sw_context;


struct util_blit_sw_context *
util_blit_sw_create(void);


void
util_blit_sw_destroy(struct util_blit_sw_context *ctx);


boolean
util_blit_sw_resource(struct util_blit_sw_context *blit,
                      struct pipe_context *pipe,
                      struct pipe_resource *src_res,
                      enum pipe_format src_format,
                      unsigned src_level,
                      int32_t src_x0, int32_t src_y0,
                      int32_t src_x1, int32_t src_y1,
                      uint32_t src_z,
                      struct pipe_resource *dst_res,
                      enum pipe_format dst_format,
                      unsigned dst_level,
                      int32_t dst_x0, int32_t dst_y0,
                      int32_t dst_x1, int32_t dst_y1,
                      uint32_t dst_z,
                      unsigned filter,
                      unsigned writemask);


void
util_blit_8unorm_rop_sw(struct util_blit_sw_context *ctx,
                        struct pipe_context *pipe,
                        struct pipe_resource *src_res,
                        int32_t src_x0, int32_t src_y0,
                        int32_t src_x1, int32_t src_y1,
                        struct pipe_resource *dst_res,
                        int32_t dst_x0, int32_t dst_y0,
                        int32_t dst_x1, int32_t dst_y1,
                        int32_t clip_x0, int32_t clip_y0,
                        int32_t clip_x1, int32_t clip_y1,
                        uint32_t rop_op,
                        uint32_t colororalphaorlogicop,
                        uint32_t flags);


#endif /* U_BLIT_SW_H_ */
